---
title: "Barcelona"
date: 2018-09-06
visitdate: 2019-05-09
day: "Thu"
draft: false
status: "started"
---

{{< h1  text="Barcelona" >}}

Arrive 7 AM; Depart 5 PM

{{< h2  text="Montserrat" >}}

(https://www.keepcalmandtravel.com/top-10-things-to-do-in-barcelona/)
