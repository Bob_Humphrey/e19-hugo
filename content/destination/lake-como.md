---
title: "Lake Como"
date: 2018-09-06
visitdate: 2019-05-24
day: "Fri"
draft: false
status: "done"
---

{{< h1  text="Lake Como (Mandello del Lario)" >}}

{{< h2  text="Apartment" >}}

Via San Zenone, 20, Mandello del Lario, Lombardia 23826, Italy

Checkin: 9 am
Checkout: 12 pm

[reservation](https://www.airbnb.com/trips/v1/4f0d5ffa-4d87-48bd-94c2-9c94e38f37f2/ro/RESERVATION2_CHECKIN/HMB38YF5XR)

The apartment is a 6 minute walk from the Mandello Del Lario train station.

Eurospin Supermercato is a discount grocery store nearby. 8 minute walk.

Spaccio Galbusera is a highly rated grocery store nearby. It's on the
highway and I'm not sure if it's walkable.

Museum Moto Guzzi - Free

{{< h2  text="Overview" >}}

Mandello del Lario or otherwise know as simply “Mandello”,  is at he foot of the Grigna moutnain. The Grigna is a popular hiking area known for its many caves and crevices as well as being host to an annual event called Sky Running- a marathon distance run where runners run UP the mountain! Winners receive the prestigious award, the Scaccabarozzi Trophy.

Cycling is also a popular sport in this area and there are many excursions and routes to take along the lake.
Tourism and Shopping

You can also visit the little church of Saint George, which legend tells , was built by a Templar monk on his way back from the Holy Land. Its walls are entirely decorated in naive style frescoes.

The surrounding towns and cities of Varenna, and Lecco are  lovely to visit, as well the larger metropolis of Milan, which is a 45 minute train ride. Milano the capital of design and fashion, has a multitude of  shopping, museums and monuments to visit.

Bellagio is a lovely lake town at the tip where the Como and Lecco branches of the lake meet. Just a ferry ride away, the world renowned lake side village has charming cafes, restaurants and boutiques, perfect for a day trip.

{{< h2  text="Wayfarer’s Path (Sentiero del Viandante)" >}}

The Path of the Wanderer, or Sentiero del Viandante, is a collection of ancient trails which traverse the east coast of Italy's Lake Como, linking Lecco to Cólico. The views from the path are beautifully picturesque - small Italian towns enveloped by a world of water and mountains.
What to See

The history of the sentiero dates back to ancient Rome and has been used by travelers, armies, pilgrims, and smugglers for centuries. This rich history is evident today, with stone fortifications, castles, and old churches still dotting the trail.

The primary feature of the hike though is its natural beauty. The views of the lake and mountains are phenomenal and constant throughout the walk. The vantage point is great too, you're high enough to look down on the red roofs of the towns and see them in perspective to the surrounding area.

Hiking along the mountainside, you'll find yourself among terraces of local gardens, bustling with olives, grapes, and so much more. Also, if you're so inclined, there are various natural points of interest along the way (e.g. Ravine of Bellano).
The Trail

The trail is well maintained with rocks embedded in the path to keep the ground sturdy. And elevation-wise the trail is generally easy to manage. The only section which may prove a bit of a challenge is the section from Lierna to Varenna which climbs up and down 2297 ft.

The trail is well marked, with plenty of signs pointing out towns along the route. In the towns, it sometimes feels as though you're walking through someone's backyard, but trudge on and you'll find your way, it's only fitting when you're on the Path of the Wanderer.

***

The wayferer’s trail is an ancient route to countries beyond the Alps, approx. 40 km length, along the eastern branch of Lake Como, ascending from Abbadia Lariana, going through many municipalities (Mandello del Lario, Varenna, Bellano, Dervio, Colico) until reaching Piantedo in Sondrio province.

You can experience it in stages and discover natural and architectural beauties, it is recommended to use proper excursion outfitting. It’s not demanding, except for some rises allowing you to enjoy extraordinary views on Lake Como and surrounding mountains.
Spring and autumn are the best seasons to enjoy it due to the favorite climate, even in sunny winter days you can have pleasant moments walking on.

The wayfarer’s trail is characterized by orange signs along the path and offers you the most attractive and evocative settings of Lake Como, together with its rich vegetation, particular geological features as well as architectural elements such as historical building, churches, hamlet and farmstead.

The trail goes through Mandello del Lario, extending both north and south. The south
leg is 3 km, about 40 minutes, ending in Abbadia Lariana.

{{< h3  text="Abbadia Lariana to Lierna" >}}

Walking time: 3 1/2 hours
Difference in altitude: + 390 meters

You start the wayfarer’s trail from the Church of San Martino, approximately 400 meters south off the railroad station of Abbadia Lariana. From here you take the path along the hamlet of Borbino and Robianico (districts of Abbadia) crossing firstly the church of San Bartolomeo, the district of Novegolo, untill you reach the church of San Giorgio in Mandello del Lario, whose origins date back to the ninth century.

Now the path goes on and rises to the district of Maggiana, where you can admire the Tower of Frederick I, called “red beard” (Federico Barbarossa), then enter into the woods to reach Rongio and go down to Valle Meria to then rise again to Sonvico.

Along the path to Galdano you can enjoy an extraordinary view of Olcio and Lake Lecco.
For a short part you have to walk close to the highway in direction to Sornico and Olgianico before finally arriving to Lierna.

{{< h3  text="Lierna to Vezio" >}}

Walking time: 4 1/2 hours
Difference in altitude: + 580 meters (732 meter top path)

In Lierna you have two possibilities to enjoy the second stage of the wayfarer’s trail: go down to the lake and take the ancient via Ducale (bottom path) or go up the mountain (top path).
Both the paths eventually join again in Varenna and are quite challenging.
The bottom path hosts the hamlet of Castello di Lierna and Fiumelatte, with its short creek mentioned by the genius of Leonardo da Vinci, while the top path rises to the plains of San Pietro offering extraordinary views on Lake Como.

Bottom path

It runs close to the lake walking from Lierna to the evocative hamlet of Castello di Lierna. Here you cross the provincial road and rise on a staircase to Ronco, where the path to Coria begins. This is the highest part of the bottom path. Here you go on walking into Valle Vacchera and then down to the lake.

There’s a walking part close the provincial road before going up the hill until reaching Fiumelatte sources and then arriving to the beautiful town of Varenna.

Top path

From Lierna rise to Genico and walk into the woods going up the hill until you reach a panoramic point, where you can enjoy an extraordinary view of Lake Como and the surrounding mountains. Then go on along the path, now easier, in direction of Alpe Mezzedo, where an ancient ice-house is visible. Going on rising to the plan of San Pietro, top of the ascent, you can really be fascinated by one of the most evocative panorama of Lake Como.

On the way to Ortanella, bypass Monte Fopp and then go down along the ridge to Colle di Vezio, firstly on the road accessible by vehicles and then on the path until you reach the Castle of Vezio, where the itinerary crosses the bottom path.

From here a steep mule-track takes you to Varenna.

{{< h3  text="Vezio to Dervio" >}}

Take the train to Varenna

From the suggestive hamlet of Varenna take the path going down to the creek Esino and go on to the ancient bridge, then rise to Regolo where you have to take the continuing path, firstly paved then dirt. Go on to Valle Masna, crossing the Woods of the Witches and then to Fabbrica. Here the path goes down to Bellano, after having run along the Chapel of Madonna Addolorata. Cross the bridge on the ravine and keep on rising to Ombriaco, reach the Sanctuary of Lezzeno, then proceed to Oro and Dervio.

Alternate description:

Along the montainuous slopes of Lake Como, an old mule track runs between the small towns of Varenna and Bellano. It allows the visitors to discover Lombardy’s typical landscapes while admiring the opposite bank of the Lake.

Varenna being the starting point of this beautiful walk, we visit the village that has kept all its authenticity. For exemple, Villa Monastero, luxurious palace with elegant gardens deserves a visit.

From the center of Varenna, we take the somewhat steep path that leads to Castello di Vezio, perched above Varenna, and first stop of the itinerary. Built in the 8th century, it is the testimony of the military fortifications in the region. But above all, from the castle’s terrace, wonderful views of Lake Como and the village of Bellagio,  on the opposite shore of the lake, can be savoured.

Below, we contemplate the red roofs of the village. The path runs along Lake Como, on the woody hills. Hiking on the Wayfarer’s path  allows us to discover bucolic landscapes: old settlements, small chapels, ancient villages and medieval bridges crossing streams.

Distance: 5 miles, duration 2h30. Return by train from Bellano.

{{< h2  text="Hiking in the Grigne Range" >}}

The eastern shore of Lake Como is bordered by the wild Grigne Range (7900 ft). Outdoor activities such as mountaineering and hiking are numerous. They offer exceptional views of the lake and its surroundings.

Starting from Lecco, we reach Piani Resinelli (4170 ft) by a winding road. This wooden plateau is the starting point of many trails towards the 2 main ranges: Grigna Settentrionale and Grigna Meridionale.

We follow the Sentiero delle Foppe, that leads to the Refuge Rosalba. The climb turns out to be somewhat steep but what a panorama at the finish! From there, we have a striking view on The Resegone, mountain with a jagged summit. The landscapes, as magnificent as wild, take on a mysterious appearance at the arrival of the fog.
