---
title: "Cadiz"
date: 2018-09-06
visitdate: 2019-05-07
day: "Tue"
draft: false
status: "done"
---

{{< h1  text="Cadiz" >}}

Arrive 10 AM; Depart 7 PM

{{< h2  text="Getting There" >}}

The ship docks in Cadiz.  It's 60 miles from there to Seville. About 2 hours each
way by train.

{{< h2  text="Cadiz" >}}

The best way to appreciate Cádiz is by wandering aimlessly. This is for two reasons. First, the city’s history of being occupied by Romans and Moors, enriched by trade with the new world, sacked by the British, fortified, and matured in the damp sea air means every one of its narrow cobbled streets spans centuries of history and has eyefuls of sights. Second, it’s impossible not to get lost (although, because it’s a small city, and bordered on three sides by beautiful beaches, not for long). Look out for the cathedral with its golden dome; the 18th-century watchtower, Torre Tavira; the Mercado Central; and the Freiduría Las Flores, the best of the city’s many fried frish joints.

Entrance to the cathedral is 5e.

Castle of Santa Catalina - free.

Mercado Central

Parque Genoves

Beaches

![map](https://e19.bob-humphrey.com/maps/cadiz-map.jpg)

{{< h3  text="More" >}}

[Cruise Critic](https://www.cruisecritic.com/ports/newport.cfm?ID=81&section=things-to-do)

(https://www.independent.co.uk/travel/48-hours-in/cadiz-travel-tips-where-to-go-and-what-to-see-in-48-hours-a6687441.html)

(https://www.theguardian.com/travel/2014/sep/27/cadiz-costa-de-la-luz-andalucia-spain-stay-eat-drink-guide)

(https://www.wanderlustchloe.com/cadiz-spain-travel-guide/)

(https://theblondeabroad.com/day-trip-to-cadiz-from-seville/)
