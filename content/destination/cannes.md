---
title: "Cannes"
date: 2018-09-06
visitdate: 2019-05-10
day: "Fri"
draft: false
status: "done"
---

{{< h1  text="Cannes" >}}

Arrive 8 AM; Depart 6 PM

{{< h2  text="Docking" >}}

Cannes is a tender port. Tenders arrive at a terrific location within easy walking distance of everything you'll want to see: shops, restaurants and the historic center of town.

{{< h2  text="Stroll along the bay of Cannes" >}}

The elegant Boulevard de la Croisette pumps with enthusiasm. If Cannes has a stage, this is it: a two-mile strip with grand hotels like the Majestic, the Carlton and the Martinez. During the film festival, when the jet set descends, the royal suite in the Majestic goes for 40,000 euros a night. Sublime, sandy beaches are attached to the hotels, but you must pay admission. Prices are listed at access points on the promenade. A small slice of public beach is located behind the Palais des Festivals. All manner of luxury brands call La Croisette home -- Cartier, Fendi, Escada, Jean Paul Gaultier, Ferragamo, Dolce & Gabbana, and Vuitton, among others. If you see a "Cannes Prestige" sign in the window, it signifies the promise of over-the-top service and at least one English-speaking sales associate.

{{< h2  text="Hand Prints" >}}

Despite its red-carpet fame as HQ of the annual film thrash, the Palais des Festivals used to have the architectural eloquence of a nuclear power station. Now bright white, it is a far better backdrop for photos of chaps in tuxedos and ladies in shiny frocks. Talking of whom, down the side of the building, very many of the most prominent movie figures have left their hand-prints set in the pavement. Once neglected, they have now renamed this stretch 'Chemin des Etoiles' and set the prints in stainless steel, giving the actors and actresses the dignity their hand-prints deserve.

{{< h2  text="Admire the view from Cannes' grand palace hotels" >}}

Amble along the seafront and revel in a prospect which has been enchanting elites for generations. The palace hotels line up like grandees on the other side of the road: the Majestic Hotel, and beyond it La Malmaison, a private 19th-century mansion open to the public; the belle époque Carlton Hotel; the more modern Palais Stéphanie (now known as the JW Marriott), incorporating the façade of the former Palais des Festivals, and the art deco Hotel Martinez. In between are gardens, the odd playground and stretches of water where you may loose a child (or husband) on the remote-controlled boats.

{{< h2  text="Historic center" >}}

Just walk uphill, and you'll reach Le Suquet, the town's historic center. The maze of about 10 narrow streets and alleyways, most of them pedestrian-only, overlooks the west end of the old port. Follow rue Saint-Antoine uphill for the best views in town. Among the sights: St. Anne's chapel, dating to the 12th century; Notre-Dame d'Esperance, a gothic church built in the 17th century; and the Castre Museum. The museum, closed Mondays, is housed in the remains of a medieval castle and features Oriental art and antiquities as well as a collection of 19th-century paintings of Cannes and the French Riviera. At the foot of Le Suquet, the famous Forville market offers a gourmet pilgrimage through the flavors of southern France. The covered market, built in 1870, is open from 7 a.m. to 1 p.m. every day except Monday when it converts to an antiques market.

{{< h2  text="Nice" >}}

30 - 40 minutes by train.  Usually 2 or more trains per hour.  Round trip for
2 is approximately $30.

Train Station – Gare des Cannes Rue Jean Jaurès, Cannes, France
is the main train station in
Cannes.  It's about 0.85 miles from the docking area used by the ship's tenders.

Take the train to the Nice-Ville station. 

{{< h2  text="More" >}}

[Tom's Port Guide](https://www.tomsportguides.com/uploads/5/8/5/4/58547429/cannes-nice-06-20-2012.pdf)

[Cruise Critic](https://www.cruisecritic.com/ports/newport.cfm?ID=182&section=things-to-do)

(https://www.kevinandamanda.com/cannes-france/)

(https://www.telegraph.co.uk/travel/destinations/europe/france/cote-d-azur/cannes/articles/cannes-attractions/)

(https://www.footstepstravelblog.com/france/cote-dazur/cannes/)
