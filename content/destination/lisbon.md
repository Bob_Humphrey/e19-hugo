---
title: "Lisbon"
date: 2018-09-06
visitdate: 2019-05-06
day: "Mon"
draft: false
status: "done"
---

{{< h1  text="Lisbon" >}}

Arrive 10 AM; Depart 4 PM

[Google Map](https://www.google.com/maps/d/edit?mid=1ZsOuFX9HsCqU6vn_tA-1V97hxZKXuKqk&ll=38.70610977142623%2C-9.183803110864346&z=15)

{{< h2  text="Entering and Leaving Port" >}}

Be sure to be on deck with your camera as your ship enters and leaves port.
You'll have great views for postcard shots.

{{< h2  text="Docks to Town" >}}

Cruise ships dock in four locations along the Tagus River.

The commercial port of Doca de Alcantara, with its traditional passenger liner terminal, lies just east (upriver) of the 25th of April bridge (a dead ringer for San Francisco's Golden Gate). It's about a 90 minute walk from here into town, but some cruise lines provide shuttle services into the center. Also, there is an efficient rail, bus and tram system -- or take a taxi to the center and to Belem.

The port of Rocha Conde Obidos is also a bit of a distance from the city center; about an hour walking distance. There's no metro or subway nearby.

A newer port with three berths, Santa Apolonia (or Alfama) is near the city center and conveniently sits next to Alfama, a neighborhood popular with visitors. Nearby, Jardim Tabaco Quay is the closest to the main city center and is the best port for walking downtown. It can take one ship.

{{< h2  text="Distances along the River" >}}

Alfama neighborhood to 25th of April Bridge - 1 hour

25th of April Bridge to Belem Tower - 45 minutes

{{< h2  text="Alfama" >}}

Alfama, the ancient Moorish quarter, is the oldest in Lisbon -- though some of it had to be rebuilt in the 18th century, following the devastating earthquake in 1755. It's fun for its cafes, little shops and labyrinthine streets. Note: The Alfama neighborhood is especially convenient if your ship docks at Santa Apolonia, but it also requires a steep climb along very narrow sidewalks and twisting cobbled streets.

{{< h2  text="Cathedral" >}}

Check out Lisbon Cathedral which dates to the 12th century. The interior features are more impressive than the exterior because the structure was rebuilt after the earthquake.

{{< h2  text="Castelo de Sao Jorge" >}}

The main event in Alfama is the 10-towered Castelo de Sao Jorge, or Castle of St. George. Built in the 10th century, then expanded into a royal palace from the 13th to 16th centuries, it's one of the few structures that survived the quake. The views down to the Baixa district, across to the Bairro Alto and out along the Tagus River are splendid. Geese and ducks roam around the castle gardens, which are surrounded by native trees. ADMISSION IS EXPENSIVE.

{{< h2  text="Rossio Square" >}}

The Baixa and Rossio districts are the central shopping and tourist areas. Several attractive parallel streets run between Rossio Square and Praca do Commercio, and it's worth picking ones with little car traffic for a peaceful roundtrip on foot. Some of the older storefronts are simply beautiful, with art nouveau facades.

{{< h2  text="Rua Augusta Arch and Praça do Comércio" >}}

Including the highly-commercialized and tourist-friendly Rua Augusta, just north of the
Praça do Comércio.

{{< h2  text="Baixa/Chiado" >}}

Baixa/Chiado is the heart of the city.
Pombaline Baixa is an elegant district and one of the first examples of earthquake resistant construction.  Buildings incorporate a wood-lattice frame
to distribute forces from earthquakes.  
In this area, you will find the Rossio or Pedro IV Square which has been one of the main squares from the
middle ages.  It's a meeting place for tourists.

{{< h2  text="Bairro Alto" >}}

The Bairro Alto is a rabbit warren of cobbled, winding alleyways with beautiful tiled houses and small squares. It's a great location for a peaceful wander during the day. (It gets livelier at night.) The hilly neighborhood is home to some of Lisbon's more intriguing restaurants. (See Lunching below.) To get there, take Tram 28 [NOTLE: LONG LINES] or, to avoid the steep climb, use the Elevador de Santa Justa at the end of the street of the same name. Opened in 1902, it runs from 7 a.m. to 11 p.m. in the summer and 7 a.m. to 9 p.m. in the winter. It often has long lines in the warmer months.

{{< h2  text="Time Out’s Spin on Mercado da Ribeira" >}}

This bold reimagining of a long-beloved market debuted in spring of 2014. While the traditional fish and produce markets in operation since the 19th century remain, top chefs and local restaurants have opened satellite stalls here, including Santini ice cream and SeaMe for prego steak sandwiches. Celia introduced me to bolo do caco, a soft, circular bread subtly flavored with garlic butter and herbs. But one of the best bites in Lisbon has to be the bolo do caco com chouriço, a chorizo-spiked sandwich I still crave. Sample them both at Bolo do Caco. The market is packed at midnight on weekend nights; go then to get the real local experience.

{{< h2  text="Walk along the Targus River" >}}

Google Maps shows a walking path from the Time Out Market to the bridge.

{{< h2  text="LX Factory" >}}

If we're near the bridge.

The LX Factory is Lisbon’s hipster hotspot, tucked away on an Alcantara backstreet. The old factory complex is home to arty stores including Ler Devager, one of Portugal’s most impressive book shops. The complex has its own rooftop bar, Rio Maravilha, with incredible views across the river.

{{< h2  text="25th of April Bridge" >}}

Built by the same engineers who built San Francisco Bridge, it's very  similar
but not exactly the same.   You need distance to appreciate the bridge and
photograph it.  Be sure to be on deck when your ship enters and leaves port.
You can also photograph the bridge from the hills of the city.  

{{< h2  text="Belem Tower" >}}

The tower was built around 1515 as part of a defense system for the city.  It was used as
 a fortress until 1580,
when Lisbon was invaded by Spanish troops.  After that, it was used primarily as a political prison.  The tower
is judged a masterpiece of the Manueline architectural style that represents the Portugal's era of exploration and
its maritime discoveries that were influential to the modern world.  It's a UNESCO World Heritage Site.  45 MINUTE WALK FROM THE BRIDGE ALONG THE RIVER.

{{< h3  text="Opening Times" >}}

Many shops and restaurants are closed between 1 and 3 PM.

{{< h3  text="Miraduoro" >}}

If you see a sign leading the way to a Miraduoro it would be a good idea to follow where it goes. These scenic overlooks can be found throughout Lisbon.

{{< h3  text="Fado" >}}

You can’t visit Lisbon without listening to the mournful fado music that the city is famous for. Solo travelers should skip the tourist traps offering dinner and a show, and head straight to the tascas: small bars where you can see a fado show for the price of a glass of wine and a bowl of the local soup, caldo verde.

{{< h3  text="More" >}}

[Tom's guide](https://www.tomsportguides.com/uploads/5/8/5/4/58547429/lisbon-06-29-2011.pdf)

[Cruise Critic](https://www.cruisecritic.com/ports/newport.cfm?ID=80&section=things-to-do)

(https://casualtravelist.com/lisbon-portugal-travel-tips/)
