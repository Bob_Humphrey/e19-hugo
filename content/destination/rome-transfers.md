---
title: "Rome Transfers"
date: 2018-09-06
draft: false
---

{{< h1  text="Rome Transfers" >}}

{{< h2  text="Airport" >}}

[train options](https://www.rometoolkit.com/airport/fiumicino_airport_train.htm)

[bus options](https://www.rometoolkit.com/airport/fiumicino_airport_bus.htm)

Take the metro from Termini to Colli Albani station

{{< h2  text="Cruise Ship" >}}

[details](https://www.rometoolkit.com/airport/civitavechia_train.htm)

About 5e per person

70 to 80 minutes

Arrives at Termini Station, the main train station in Rome

Take the metro from Termini to Colli Albani station

Cruise ships are supposed to have free shuttles to a cruise ship reception area called Largo della Pace just outside one of the dock gates.

From there it is a 15 minute walk to the train station.

{{< h3  text="Walking to the train station" >}}

The route is pretty straightforward with pretty smooth rights of way which you can roll your luggage along.

From the front entrance of the railway station turn right down the small access road to the wide coastal road and cross this road immediately to the promenade and turn right towards the centre of town.

The promenade footpath is very wide, parallel to the sea and very smooth without kerbs to negotiate. After about 5 minutes you come to the dock gates with a McDonald's on your right and the imposing Fort Michelangelo on your left.

Walk through the gates and immediately cross to the right hand side of the road. Now just follow the dock side, ignoring the spur that goes out to the left immediately after entering through the dock gates.

Now just follow the pedestrian path as it winds through the docks past fishing boats, a terminal with toilets and shops. Most of the way the path is just a cordoned off strip of tarmac separate to the road and again the surface is very smooth. Eventually you come to a road junction by another set of dock gates and you just follow the pedestrian signs out of the docks labelled “Shuttle Bus To Cruise” to the Largo della Pace facility on the opposite side of the road to the port exit.

{{< h3  text="Train to Rome" >}}

Both Civitavecchia and Rome Stations have manned ticket offices that take credit cards and there are also ticket machines. Ticket machines are easy to use, take all major credit cards and support several languages including English.

Most frequencies between Civitavecchia and Rome are by local stopping train. There are infrequent inter-city trains that run non-stop between Civitavecchia and Termini Station, in the Civitavecchia to Rome direction punctuality especially is not good.

This is not Switzerland, do give yourself plenty of contingency - the trains are reliable but are subject to delays.

**Beware, if you buy a ticket for the local trains it is flexible and you have to stamp it before you board the train at machines around all the stations. You risk a heavy fine if you produce an unstamped ticket for a ticket inspector. Further details of this are on our main Rome public transport page.**

You can also purchase tickets in advance on the Tren Italia web site.

Note: Fares on The TrenItalia site for Civitavecchia local trains are only displayed up to seven days in advance. Inter city trains, 4 months.  
