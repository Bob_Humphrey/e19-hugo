---
title: "Fiesole (Florence day trip)"
date: 2018-09-06
visitdate: 2019-05-18
day: "Sat"
draft: false
status: "done"
---

{{< h1  text="Fiesole" >}}

Older than Florence and overlooking it from on high, the Etruscan village of Fiesole has a few sights, cafés on the main square, and best of all, a cool mountain breeze even on the hottest August days.

Most of what people come to Fiesole to do is simply wander the streets for a break from the big city sights and a taste of small town Italy. But, as you stroll, do take the time to pop into a few of the the town's (admittedly modest) sights.

-------

If you’re looking for the perfect day trip from Florence, head to Fiesole, a village that has stolen hearts from many locals and tourist alike. Tucked in the northern Tuscan hills, it’s about an hour’s walk (or a quick drive) from bella Firenze. And its extraordinary views of Florence mean that it’s a top-notch spot for those wanting that perfect photo op of the Florence Duomo!

To get there without a car or a hike, just take the ATAF 17 bus, then the 7, to Fiesole (40 minutes in total). You’ll arrive in the heart of town in Piazza Mino, where there is usually a local market during the weekends. Bring cash so you can leave with some local food, wine and artisan treasures.

{{< h2  text="How to reach Fiesole by public transport" >}}

The ATAF Florence city bus to Fiesole is Number 7 which runs about every half an hour until almost midnight so that it is possible to remain in Fiesole for dinner. The bus line starts at Via La Pira (on the side very close to Piazza San Marco) in Florence.

Luckily, Piazza San Marco is one end of this bus line in Florence, and Fiesole is the other end, so you don't have to worry about getting on in the right direction—or when to get off.


{{< h2  text="Vistas and churches" >}}

Fiesole's 11th-century Cathedral contains some delicate Mino da Fiesole carvings. The Palazzo Vescovile (Bishop's Palace) nearby contains the Cappella di San Jacopo Maggiore, with a lovely, early 15th-century Coronation of the Virgin fresco by Bicci di Lorenzo.

Make your way up (and I do mean up) Via San Francesco to the tiny terrace of panoramic gardens ★★ overlooking a postcard view of Florence down in the valley. There are usually several people with easels set up under the holm oak trees, capturing the scene in watercolor.

If you continue up the hill from here, you pass on your right the church of Sant'Alessandro, which has a few lithe columns of cipollino marble recycled from ancient Roman buildings.

The steep road ends at the church and friary of San Francesco ★, which has a Gothic altarpiece painted by Neri di Bicci, and in the nice cloisters the remains of the 3rd century BC Etruscan wall and a small missionary museum containing works you wouldn't necessarily expect in a Tuscan village: statues from ancient Egypt and Ming and Qing vases from China.

{{< h2  text="Fiesole’s stunning monastery and views" >}}

Work up an appetite by walking up to the Monastery of San Francesco. It’s the steepest street in the area, but the views are the payoff!

Besides visiting the adjacent chapel and free museum that is run my the monks, you’ll see some of the most spectacular views of Florence. There are park benches that gaze over the terrace; it’s the perfect place to read or have a little picnic.

To head back down to the center of town take the little path in front of the chapel. It’s a beautiful path through a park, just another spot to take walks and enjoy the scenery.

{{< h2  text="Hiking from Fiesole to Florence" >}}

My favorite way to get back down to Florence from Fiesole is by foot.

You can leave the main piazza on Via Beato Angelico, keeping left at the bend to hike down ★ along cypress-lined Via Vecchia Fiesolana, passing walled gardens.

Most villas in and around Fiesole are open to the public—one at a time on a rotating schedule—Thursdays around 4pm from mid-April though early October. (See www.fiesoleforyou.it for this summer's schedule of villa openings.) You must book a visit by the preceding Monday by calling tel. +39-055-055 weekdays between 9am and noon.

On weekdays you can visit the gardens of Villa Medici, first gate on your left (Mon–Fri 9am–1pm; tel. +39-055-59-164 or 055-59-417); and the gardens at Villa Le Balze at no. 26 (by calling ahead at least two days in advance: tel. +39-055-59-208).

At the hamlet of San Domenico halfway down sits the monastery where Fra' Angelico first took his vows; the Chapter House (ring at no. 4) preserves two of his paintings, with another in the church itself.

Here you can hop the no. 7 bus again to ride back through Florence's uninteresting outskirts to the center.
