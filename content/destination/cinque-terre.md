---
title: "Cinque Terre"
date: 2018-09-06
visitdate: 2019-05-21
day: "Tue"
draft: false
status: "done"
---

{{< h1  text="Cinque Terre" >}}

{{< h2  text="Train" >}}

Florence to La Spezia - 2.5 hours

50 minute walk from the train station - 10 to 20 minutes by bus

Stazione La Spezia Centrale, Piazzale Medaglie d'Oro, 19122 La Spezia SP, Italy

![map](https://e19.bob-humphrey.com/maps/la-spezia.png)

[Google Map](https://www.google.com/maps/d/edit?mid=11uiBCi0hcP3PcoHEqJqGlY1YaGgpUA7e&ll=44.09992089292697%2C9.816274499999963&z=14)

{{< h2  text="Apartment" >}}

Via Nicolò Fieschi, 252  La Spezia, Liguria 19132

Checkin: 3 PM

Checkout: 10 AM

[reservation](https://www.airbnb.com/reservation/itinerary?code=HM5CNX8RNM)

{{< h2  text="General" >}}

crowded during the day, more relaxed after 4:00

villages are expensive

Sit-down meals in this region are very expensive. Stick to cheap sandwiches and pizza if you want to save money.

Walking: the first two sections, connecting Riomaggiore to Manarola and Manarola to Corniglia, are closed due to landslide (a reopening date has not been set). In-land trails are available but require a good level of fitness or hiking expertise.

Cinque Terre is famously crowded, so be prepared for crowds during your visit. The good news is that the tour groups congregate on the central streets of each town from about 10 a.m. to 4 p.m. during the high season. In the early morning and evenings, the pace slows down and you can experience the small Italian town feeling. Even mid-day, it’s possible to get away from the crowds on some of the side streets.

Be prepared to walk. These towns are pedestrian-friendly only. Not only are there virtually no options for assisted transportation, but the towns themselves are hilly, so it’s common to encounter stairs—sometimes a hundred at a time. If you have mobility problems, consult with your chosen accommodation to find out if they are accessible and if they can assist you with bags or any other needs.

Be aware that the hiking trails that are currently open are quite challenging. Many visitors have a romantic vision of walking between the villages, but unfortunately, the easiest stretch of the coastal trail is closed for the long term due to landslide. Trails can and should be taken at your own pace, and though being a regular hiker is not a prerequisite, being active and in good health is. Paths often begin with a strong vertical and can be narrow at times. Always hike with a bottle of water, and in the summertime, a hat and sunscreen. There are no services along the trail.

{{< h2  text="Cinque Terre Card" >}}

- includes unlimited train travel

- one day 16e, two day 29e

- can buy at all area train stations

- train from La Spezia to Levanto

- stops at every village

- if you are not walking the trails, you don't need the card; single train
tickets are 4e for each time you board the train

- [card info](http://www.cinqueterre.eu.com/en/cinque-terre-card)

- [schedule](http://www.cinqueterre.eu.com/en/cinque-terre-timetable)

{{< h2  text="Blue path, Sentiero azzurro" >}}

The Sentiero Azzurro connects the five villages of the Cinque Terre, and it is the most famous and appreciated path as it does not consist in particular difficulties. It is almost flat, nevertheless you can enjoy fantastic panoramas. It is possible to cover the whole path, which connects the five villages, by foot going along the path No. 2 (marked in white and red) from Monterosso to Riomaggiore (12 Km) .

The total time taken for the journey is about 5 hours, but obviously the way can be divided in halting-places in every village.

- MANAROLA - RIOMAGGIORE, Difficulty: easy, Length: 1,5 km, Duration: 30 minutes,
The famous Via dell’Amore, "Lover’s Lane", starts at the railway station of Manarola with stairs. It is the most popular stretch of path of the Cinque Terre.

- CORNIGLIA - MANAROLA, Difficulty: easy, Length: 3 km, Duration: 1 h, 377 steps

- VERNAZZA - CORNIGLIA, Difficulty: average, Length: 4 km, Duration: 1,5h – 2h

- MONTEROSSO - VERNAZZA, Difficulty: average, Length: 3,5 km, Duration: 1,5 h

{{< h2  text="High path, Sentiero del Crinale" >}}

The high path (no. 1) is an old mule-track, perhaps dating back to Roman times, which runs along the ridge which separates the coast from the hinterland.

The complete route, the major part of which is inside the Cinque Terre, starts in Levanto, on the west and terminates at Portovenere on the east, lasting for over 40 kilometres and touching quotas of 800 metres with a total difference in levels of about 1.300 metres

{{< h2  text="How hard are the walks?" >}}

For centuries the only way you could get between the Cinque Terre villages was on foot, and it’s still the best way to get around, with a constant stream of gorgeous views. There’s a mix of coastal and hill paths. Though the coast paths aren’t just a walk along the seafront – at least not the part that’s open. The one flat stretch of coast path from Corniglia to Riomaggiore is closed for the foreseeable future after landslips damaged it back in 2011. The rest of the paths involve lots of ups and downs with some rocky ground and a few big drops and steps to clamber up. You don’t need to be really fit but do need to be be comfortable walking uphill and have decent walking shoes – most people were wearing hiking boots or sturdy trainers.

[https://www.ontheluce.com/cinque-terre-italy-guide/]


{{< h2  text="Travel Blogs" >}}

- [https://www.ontheluce.com/cinque-terre-italy-guide/]

- [https://www.polkadotpassport.com/the-ultimate-guide-to-visiting-cinque-terre/]

- [https://www.italianfix.com/cinque-terre/]
