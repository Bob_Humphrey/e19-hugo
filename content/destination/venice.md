---
title: "Venice"
date: 2018-09-06
visitdate: 2019-05-27
day: "Mon"
draft: false
status: "done"
---

{{< h1  text="Venice" >}}

{{< h2  text="Apartment" >}}

Via Felice Cavallotti, 11 appartamento, Venice, Veneto 30171, Italy

Checkin: 5 pm

Checkout: 10 am

[reservation](https://www.airbnb.com/trips/v1/27a0e356-6ac6-485e-b116-1cd3dc0ed1ec/ro/RESERVATION2_CHECKIN/HMNKQEQKKC)

The house is located near the Mestre train station. Opposite the station there is a street called Via Piave, follow this street for about 500 meters and on the right you will find via Cavallotti. turn right, the house number is 11.

8 minute walk from Venezia-Mestre train station.

Alìper Ipermercato supermarket is nearby with good reviews.  4 minute walk.

Bus stop 2 minutes away.  Hostess can provide instructions.

{{< h2  text="Avoiding the Crowds" >}}

-- Burano

--  Wear comfortable shoes and spend the day walking to the lesser-known areas of Venice. A few of my favorite neighborhoods include Cannaregio, Santa Croce, and Dorsoduro.

{{< h2  text="Jewish Ghetto" >}}

If you don’t fancy taking a ferry out to one of the other islands of Venice, or if you are looking for a destination that is close to the train station, visit the Jewish Quarter. Even though it is located close to the main tourist areas of Venice, it is a very quiet and charming quarter.

It’s best to join a walking tour to understand better the background and history of the Jewish community in Venice. It is indeed the oldest ghetto in the world and has some of the very first “skyscrapers” in Europe.

Have a look at the historic synagogues, visit the museum of the Jewish Ghetto, and maybe also drop into one of the art galleries. Try some of the specialties in the bakeries and delis, shop for souvenirs in the artisan workshops, and see one of the most astonishing and least known sights of Venice, the bas-reliefs in the Campo del Ghetto Nuovo.

Visiting the Jewish Ghetto in Venice is a slightly somber, yet very different and unique experience which will open up fresh and very unusual perspectives of the lagoon city.

{{< h2  text="Mingle with the Locals in Cannaregio" >}}

Surrounding the Jewish Ghetto (see above) is the wider area of Cannaregio in the north of the main island. It’s a place that sees far less visitors and where locals still come together to dine in small restaurants along the quiet canals. The bars are populated by young locals, in particular during happy hour.

Besides the obvious advantages of not being overrun by tourists, there are a couple of noteworthy highlights that you can explore in Cannaregio in your own time.

For example, check out Fondamenta della Misericordia for its string of wonderful canal-side restaurants where you can dine off great local dishes such as liver with onions and spaghetti with cockles.

Also make sure you pass by Campo dei Mori with its very unique statue of moors, clad in traditional Arab clothes such as turbans and and long dress-like clothes.
