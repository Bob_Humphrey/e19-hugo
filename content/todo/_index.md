---
title: "Todo"
date: 2018-09-06
draft: false
---


{{< h1  text="To Do" >}}

- railroad tickets

- money belt

- euros from aaa

{{< h2  text="Cannes">}}

- consider including Nice

{{< h2  text="Livorno">}}

- decide/book trip to Pisa and/or Lucca

{{< h2  text="Rome" >}}

- tickets for papal Audience

- tickets for Vatican Museums

- public transportation details

{{< h2  text="Cinque Terra">}}

- find out what walking paths are open (some were destroyed by landslides in 2011)
