---
title: "Lucca (Florence day trip)"
date: 2018-09-06
visitdate: 2019-05-19
day: "Sun"
draft: false
status: "done"
---

{{< h1  text="Lucca" >}}

Encircled by an imposing ring of tree-lined Renaissance walls that you can walk or cycle along, Lucca is situated in a fertile plain near the Tyrrhenian Sea, part the way between Pisa and Florence.

If you’re staying in Florence, the easiest way to get to Lucca is by train, from the city’s Santa Maria Novella station (which is walkable from almost anywhere in central Florence).  Direct trains run every hour, even on a Sunday, and the price of a return journey is €15.  We caught the 10:10 train, which dropped us into Lucca at 11:29.

Lucca’s train station is literally the other side of the 16th century ramparts to its historic centre.  So, once you exit the station, just walk across Piazzale Bettino Ricasoli, and follow the path across the old moat.  Alternatively, follow everyone else who gets off the train; most people are heading in the same direction!  Don’t forget to stop off at the tourist information centre to pick up a map.

Lucca’s tall buildings and largely traffic-free (save for the odd push bike) narrow, cobbled streets mean that its the perfect place for an aimless wander.  Its labyrinthine alleyways are punctuated with beautiful churches and hidden piazzas containing cute little cafes and boutique shops.

Unlike Florence, where there are certain must-see sights that feature on every traveller’s itinerary, the whole of Lucca’s old city is like a living museum and is guaranteed to totally charm your socks off.  It’s also pretty compact so is easily walkable in a day.

{{< h2  text="Walk or cycle along the city walls" >}}

Lucca’s monumental mura (wall) was built around the old city in the 16th and 17th centuries, and is one of the best preserved city walls in all of Tuscany.

You can walk or cycle around the entire 4.2 kilometre circumference, along the beautiful tree-lined ramparts that overlook the centro historico in one direction, and the Apuane Alps in the other.
