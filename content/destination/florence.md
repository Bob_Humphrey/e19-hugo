---
title: "Florence"
date: 2018-09-06
visitdate: 2019-05-16
day: "Thu"
draft: false
status: "done"
---

{{< h1  text="Florence" >}}

{{< h2  text="Apartment" >}}

Via Fratelli Cairoli, 90, Sesto Fiorentino, Toscana 50019, Italy

Checkin: 4 pm

Checkout: 10 am

[reservation](https://www.airbnb.com/trips/v1/e865940d-cf27-41c2-8c23-3f14ac4f0dcf)

If you arrive by train at Sesto Fiorentino station.
Go out of the station side platform 1
Follow Viale G. Ferraris (the tree-lined street in front of the station).
Turn right (Via G. Garibaldi).
After 200 meters on the left you will find via Fratelli Cairoli.
On the bell at number 90, you play at Cerruti Pinna

{{< h2  text="Central city" >}}

Wander the city and admire the architecture from the outside.

{{< h3  text="Loggia dei Lanzi and Piazza della Signoria" >}}

One of the most important squares in the city offers lots to see, as well as offering a great place to sit and people-watch, preferably with a gelato in hand (several recommended gelateria in the area - check out our list here). The statues in the Loggia dei Lanzi, along the front of Palazzo Vecchio and in Piazza della Signoria turn the square into an open-air museum that is completely free where you can admire works by greats such as Michelangelo, Giambologna, Cellini, Bandinelli and Ammannati.

{{< h3  text="Oblate Library Terrace" >}}

Free

This public library right behind the Duomo has a top floor terrace with a great view. The cafeteria there also offers a perfect occasion for a light lunch or aperitivo with cocktail in hand and view to admire.

{{< h2  text="South bank of the Arno" >}}

Most tourists stay on the northern side of the city – where the Duomo is located. Personally, I prefer the other side of the river as it feels more local. The Santo Spirito and San Niccolo areas are where you’ll fall in love with Florence.

Cobblestone streets dotted with flower pots, traditional Italian coffee shops, and young locals hanging out at Piazzas in the summer.

The south bank of the Arno is where the young, alternative, bo-ho type Florentines tend to congregate. You’ll find some of the best bars, clubs, restaurants, book stores, hostels etc here. Make sure to check out La Cite – one of the funkier book store/ cafe’s/live music venues in the whole city and Pop Cafe which has one of the best aperitivo’s (see ‘Drop In’ below) in the city but is also the epicentre of the city’s blossoming counter-cultural scene.

{{< h2  text=" Giardino dell Orticultura" >}}

FREE

Head north of Il Duomo on Il Cavour and cross the outer ring of the old City. Cross Piazza Liberta and keep heading north until you see a sign for the Giardino dell Orticultura. This beautiful park & open air garden stretches into the hills that overlook Firenze. It is a true slice of beauty and tranquility and if it wasn’t for the amazing views of the old town you might just forget what city you’re in.

 Featuring a huge nineteenth century greenhouse, it hosts flower markets, and during summer it becomes an outdoor pub, with a kiosk, and concerts every night.

 {{< h2  text="Piazza Michelangelo" >}}

 FREE

 Piazza Michelangelo is on a hill on the south bank of the Arno River, just east of the center of Florence. Designed in 1869 by Poggi, it offers a great view of the city, and is a very popular tourist destination.

  Far and away the most beautiful view of the Florence skyline. Perched on a hill in the southeast, the Piazza Michelangelo is built around a huge bronze replica of Mikey-Angel’s dashing Davie. This is a great place to come day or night with a bottle’a red (but you didn’t hear that from me). To really get your money’s worth try getting to the Piazza via one of the many paths leading up to it. Inhale….. then exhale.

 Best to visit at sunset.

 Piazzale Michelangelo gets pretty busy, particularly just before sunset, so take your photos, and then cross the road and continue uphill to the Abbazia di San Miniato al Monte.

 This is one of the few churches in Florence that’s free to enter.  And I totally recommend that you do; the interior is utterly beautiful.

 A stop most don't ever think to make up here is to head back behind the church and enter into the cemetery which surrounds the entire church. As you'll already have seen from the front parts of the church, there is something special about this cemetery which hosts the tombs of important Italians tied to the history of Florence as well as interesting neo-Gothic tombs.

There are also far fewer tourists up here, and although Piazzale Michelangelo is probably a better place to take your bridge shots from, the views from here are otherwise just as good.

 {{< h2  text="Giardino delle Rose" >}}

 FREE

 This peculiar and quiet terraced garden, situated on a hillside just a few steps away from Piazzale Michelangelo, offers another magnificent view of Florence and it is a beautiful place to relax.

 {{< h2  text="Basilica di San Miniato al Monte at dusk" >}}

Everyone tells you that Piazzale Michelangelo is THE place to watch the sunset over Florence but in my opinion the views from the Basilica di San Miniato are just as good, and there are far fewer people.  It’s also free to enter the basilica, and it’s utterly beautiful inside.

{{< h2  text="River Arno" >}}

Wander along the River Arno to marvel at Florence’s iconic Ponte Vecchio.  

Yes it’s probably the most touristy activity in Florence (along with visiting the Duomo), but the bridge does have a certain colourful, rambling appeal.

{{< h2  text="Day Trips" >}}

- Fiesole
- Lucca
- Siena

{{< h2  text="Local Busses" >}}

[details](https://www.visitflorence.com/moving-around-florence/by-bus.html)

Tickets are valid for 90 minutes and cost €1,50 each. A 4-ride ticket costs € 4,70.

An "I didn't know" will not get you off the hook if you get caught without a stamped ticket. The fine is minimum 40 euros if you pay on the spot.

 Ordinary (a single use 90 minute ticket) and multiple-ride tickets (four 90 minute rides on one single ticket) can be purchased from authorized sales points (bars which are coffee shops here, tobacconists, newsagents: anyone with "ATAF" stickers on their shop windows) and from the ATAF booth within the SMN train station.

 Buy your tickets BEFORE you board the bus, as the bus driver does NOT check tickets and let's everyone on without asking for a ticket. You can buy tickets from the driver but they cost more - and since they often run out of tickets, there is no guarantee you can get tickets from him/her!

 There are three doors on the ATAF buses, the front and back ones are for getting on and the middle one is for getting off. After you get on, make your way to the nearby "ticket validating" machine and put your bus ticket in. The date and time is then printed on the ticket, starting the time validity of your ticket. You basically have 90 minutes on your ticket, so you could get on and off onto different buses and you use the same ticket.

{{< h2  text="Regional Busses" >}}

Tourists interested in seeing areas outside of Florence by bus will find all of the bus companies in the area around the Santa Maria Novella train station. The main lines offering services are: CAP, FlorentiaBus, Lazzi and Sita (now called BusItalia). Some lines service particular areas around Florence, so depending on where you want to go, you might have to go with a particular company. Check the schedules on their sites to see which one is the one you'll need to take - in general, CAP takes you toward Prato, Lazzi to Pistoia, Valdarno and Lucca, Florentia is for longer distance trips but also runs the bus to the McArthur Glen Shopping Outlets in Barberino and SITA runs most buses to Chianti, Mugello, Casentino and Siena in cooperation with other companies.

{{< h2  text="Train Station" >}}

The city's main railway station is Firenze Santa Maria Novella (abbreviated as Firenze SMN). Situated in the city center, it is conveniently close to the major tourist attractions as well as to the main exhibition and trade centers. T

{{< h2  text="Articles" >}}

[Free things to do](https://galloparoundtheglobe.com/a-few-things-to-see-and-do-in-florence-for-free/)

[Itineraries in Florence](https://www.visitflorence.com/itineraries-in-florence/)

[Free things to do in Florence](https://www.visitflorence.com/itineraries-in-florence/florence-for-free.html)

[8 Day Trips in Tuscany from Florence](https://www.visitflorence.com/itineraries-in-florence/top-day-trips-from-florence.html)
