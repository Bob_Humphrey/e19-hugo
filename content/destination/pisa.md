---
title: "Livorno"
date: 2018-09-06
visitdate: 2019-05-11
day: "Sat"
draft: false
status: "done"
---

{{< h1  text="Livorno" >}}

Arrive 7 AM; Depart 7 PM

{{< h2  text="Port" >}}

[Tom's Port Guide](https://www.tomsportguides.com/uploads/5/8/5/4/58547429/livorno-09-25-2013.pdf)

All cruise ships dock in the
industrial port area.  This area is unsafe for pedestrians.  You
cannot walk in the industrial port area and it's impractical to
walk because it is 4.4 to 8 km to center city depending on
where your ship is docked.  The only way to leave or return
to your ship is in a vehicle.  

If you are doing self-guided touring:

- Your ship should provide bus transport to
Piazza del Municipio in center city
 Livorno.  

- The service is free on some cruise lines, but most charge from 5 euros to $10 round trip.  It's a 10-15 minute bus ride.

- Some ships “hold back” independent travelers and do not start the shuttle bus service until all of the ship's shore excursion buses have departed.  Also, there may be long lines for the first shuttle bus.  It's unlikely you will arrive in Piazza del Municipio
center city Livorno before 9:00 or 9:30 am.  From the shuttle bus stop, you can explore
Livorno on foot, but you'll need to take a public bus to the Livorno Centrale Train Station.  

- Some ships offer a shuttle bus from the dock to Livorno Centrale Train Station for ~ $ 24 round trip.

{{< h2  text="Tourist Information" >}}

There is a tourist information office on the southeast corner of Piazza del Municipio,
across the street from McDonalds.  May have information for taking the bus to Pisa and/or
Lucca.

{{< h2  text="Bus to Pisa and Lucca" >}}

- (http://www.tuscanybus.com/lucca-pisa/)

- can buy tickets online; 28 e per person; there's a cheaper Pisa-only
option

- The departure is in Via Cogorano next to the shuttle bus stop linking the port of Livorno with the city center.

- guarantees return 90 minutes before ship leaves

- 10:30 depart Livorno; 11:30 arrive Lucca; 2:30 depart Lucca; 3:00 arrive Pisa;
4:30 depart Pisa; 5:10 arrive Livorno

{{< h2  text="Train Station" >}}

Livorno Centrale Train Station is a 30 minute walk (2.4 km) from Piazza del Municipio.

{{< h2  text="Train to Lucca" >}}

- 2 trains per hour each way; transfer in Pisa

- $27 (1 adult and 1 senior) if you catch the train on or before 9:19 AM; during the 10:00 and
11:00 hours there is only one train per hour, and the fare is $7.50 higher

- You can take a train to Pisa, see the sites, and take a second train to Lucca
for the same price.

{{< h2  text="Pisa" >}}

Pisa’s Leaning Tower is touristy but worth a visit. Many tourists are surprised to see that the iconic tower is only a small part of a gleaming white architectural complex — featuring a massive cathedral and baptistery — that dominates the grand green square, the Field of Miracles. The rest of the city is virtually tourist-free and merits a wander for its rich history, architecture, and student vibe. (Rick Steves)

{{< h3  text="Duomo" >}}

The Cathedral (Duomo): If you’re not all cathedral-ed out (ti can happen pretty quickly in Europe), the Duomo is substantial and stunning – it was the biggest in Europe at the time when it was built.  Entry into the Cathedral is free.

{{< h3  text="Blue Palace" >}}

Palazzo Blu (the Blue Palace) lies along the river in the historic center. It is home to over 300 artworks ranging from the 14th to 20th centuries. Entry is free and open daily (except Mondays) from 10am-7pm with extended hours on the weekends.

{{< h3  text="More" >}}

(https://blog.ricksteves.com/cameron/2016/05/pisa-beyond-tipsy-towers/)

{{< h2  text="Lucca" >}}

Surrounded by well-preserved ramparts, layered with history, alternately quaint and urbane, Lucca charms its visitors. Though it hasn’t been involved in a war since 1430, it is Italy’s most impressive fortress city, encircled by a perfectly intact wall that’s perfect for a laid-back bike ride — the single must-do activity in this pleasant getaway. It’s simply a uniquely human and undamaged, never-bombed city. Romanesque churches seem to be around every corner, as do fun-loving and shady piazzas filled with soccer-playing children. Locals say Lucca is like a cake with a cherry filling in the middle...every slice is equally good. (Rick Steves)

Lucca is a mid-sized city (of around 90,000 people) on the northwestern edge of Tuscany. It’s about a 30-minute drive or train ride from the tourist droves in Pisa. But somehow, Lucca has escaped everybody’s notice.

It’s not for lack of charm. Lucca is right up there on a list of most charming Tuscan cities. Frankly, great artwork aside, I’d rank it above Florence, and possibly even Siena.

And that’s probably Lucca’s secret: No world-class artwork. If there were a Michelangelo or a Leaning Tower here, Lucca would be an obligatory stop on the tourist circuit. But there isn’t…so it’s not.

Lucca does have some gorgeous churches, and a few decent museums. But the city’s real draw is its everyday-ness. It’s a place still owned and operated by local people — not the tourist-industrial complex. It’s simply a delight to wander.

The big landmarks are the rampart park that surrounds the city center (you can bike all the way around in under a half-hour), a couple of piazzas with towering churches, and an oblong square that echoes the footprint of a Colosseum-like arena that once stood here.

But the real joy of Lucca is simply wandering its streets. Despite its approximately regular grid plan, the city is a maze. I get lost here more than in any town in Italy. But maybe, subconsciously, that’s intentional — few places are more enjoyable to simply be lost.

(https://blog.ricksteves.com/cameron/2016/05/untouristy-lucca/)

{{< h3  text="More" >}}

(https://notanomadblog.com/ten-reasons-to-love-lucca-italy/)

(https://blog.ricksteves.com/blog/travel-bite-beautifully-preserved-lucca-italy/)

(https://www.travelsewhere.net/lucca-italy/)
