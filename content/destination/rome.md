---
title: "Rome"
date: 2018-09-06
visitdate: 2019-05-12
day: "Sun"
draft: false
status: "started"
---

{{< h1  text="Rome" >}}

{{< h2  text="Arriving in Rome" >}}

[details](https://e19.bob-humphrey.com/destination/rome-transfers/)

{{< h2  text="Apartment" >}}

Via Castel Gandolfo 71, Rome, Lazio 00179

Checkin: 2pm

Checkout: 10am

3.5e per person per day tax must be paid in cash

[reservation](https://www.airbnb.com/reservation/itinerary?code=HMES8Z2D4T)

Close to COLLI ALBANI station, Metro line A

Basilica of San Clemente (the start of the historic section) is about a 50
minute walk from the Apartment. The Colloseum is an hour.  The Trevi Fountain
is an hour 20 minutes.

{{< h2  text="Metro and Busses" >}}

[details](https://www.rometoolkit.com/transport/rome_travel_pass.htm)

- Buy tickets at newsstands or bars.
- Have ticket stamped when you get on the bus.
- Exit through the double doors in the middle.
- Same rules apply for all of Italy.

{{< h2  text="Vatican Museums" >}}

- includes the Sistine Chapel

- open at 9

- should be in line by 7:30

- [buy tickets](http://www.museivaticani.va/content/museivaticani/en/visita-i-musei/scegli-la-visita/visitatori-singoli.html#lnav_shop)

- long pants required, no shorts


{{< h2  text="St. Peter's Basilica" >}}

- open at 7 am

- free, but the lines are very long; get there early

- closed on Wednesday mornings for the Papal Audience

- Michelangelo's Pieta




{{< h2  text="Papal Audience" >}}

- Wednesday at 10:30am

- Free but must have tickets

- Having a ticket does not guarantee entrance.  Get there early.  The
doors open around 8am.

- [Schedule of Pope's public events](http://www.vatican.va/various/prefettura/en/udienze_en.html)

- [Faxing for tickets](http://www.vatican.va/various/prefettura/index_en.html)

- [More info ](https://theromanguy.com/italy-travel-blog/how-to-get-tickets-for-papal-audience-rome/)

{{< h2  text="Near Apartment" >}}

- Appian Way in Parco della Caffarella, next to our apt.


{{< h2  text="Central Area" >}}

- bike path along the river
- Colloseum
- Trevi Fountain
- Spanish Steps
- Pantheon
- Orange Garden on Aventine Hill
- Protestant Cemetery
- Castel Sant’Angelo
- Piazza Navona

- San Clemente - A few blocks from the Colosseum is the best place in Rome to see the layer-cake effect of its history in action: church piled atop church piled atop pagan temple

- The Mouth of Truth—Santa Maria in Cosmedin (from Roman Holiday)

- Santa Maria del Popolo - Though virtually ignored by the Rome's teeming crowds of tourists, it's perhaps my favorite out of all the city's nearly one thousand churches. This little gem acts as a primer of Italy's Renaissance and early baroque movements, with examples in all the artistic disciplines—painting, architecture, fresco, sculpture, mosaics, and stained-glass—from various eras and by the very top names in the Old Masters game.

- Piazza del Popolo - near the church above

- Campo de' Fiori

{{< h2  text="Trastevere" >}}

- Santa Maria in Trastevere

- Santa Cecilia in Trastevere

- The Gianicolo - The Janiculum Hill (Gianicolo) is one my favorite spots in all of Rome, Italy. Rising above Trastevere, south of the Vatican, is a long ridge paralleling the Tiber called the Gianicolo (Janiculum), which is famously not one of the Seven Hills of Rome.
There are a few sights up here, but the most attractive feature is simply the sweeping view of Rome across the river, taking in everything from the Pincio gardens of the Villa Borghese on the left past the domes of the city center beyond the curve of the Colosseum on the right.

The popular Trastevere district (Rome's 'left bank') is a postcard neighborhood of narrow, cobblestone streets, fantastic restaurants, and overflowing bars and clubs

It's still as picturesque as any corner of Rome comes: a warren of narrow, cobblestone-paved streets and alleys that twist and turn and suddenly spill into pocket-sized piazze filled with market stalls or anchored by a medieval church, laundry hanging from marble windowsills set in ochre or gold or pale rose plastered walls, and tiny shops, boutiques, and trattorie pigeonholed into the ground floors of every building.




{{< h2  text="Near Rome Termini Station" >}}

- Basilica di Santa Maria Maggiore




{{< h2  text="Blogs" >}}

- [https://theromanguy.com/italy-travel-blog/]
- [Rome in 3 days](http://www.reidsitaly.com/destinations/lazio/rome/sights/rome_itin_3D.html)
