---
title: "Ponta Delgada"
date: 2018-09-06
visitdate: 2019-05-04
day: "Sat"
draft: false
status: "done"
---

{{< h1  text="Ponta Delgada" >}}

Arrive 7 AM; Depart 3 PM

It's the first port after 5 long sea days, and you're going to be
excited to be back in Europe.  But there's not a whole lot to do here.
The day will be spent wandering around this small town, exploring
the streets and churches and parks. Most of the activities on this
island involve taking planned excursions to sites like gelysers,  
hot springs, tea plantations, and a pineapple plantation.

{{< h2  text="Docks" >}}

There are two dock areas in Ponta Delgada.

1)
The first cruise ship to arrive docks at the main cruise ship dock GPS =
N 37°44′17.3′′, W 25°39′44.9′′
The dock is next to 37 harbor shops, bars, restaurants, and businesses plus the dock for ferry boats to the other
Azores Islands.  It's a 750 foot walk to Avenida Infante Dom Henrique, the main road along the harbor.

2)
The second cruise ship to arrive ties up at the industrial docks.   GPS =
N 37°44′06.6′′, W 25°39′41.7′′
It's 3900 feet from the cruise ship to Avenida Infante Dom Henrique, the main road along the harbor.  However,
you cannot walk through the industrial docks when cargo is being loaded on/off container ships.  
Free shuttle
buses are provided from the ship to
Forte de São Brás/Military Musem.

{{< h2  text="Town of Ponta Delgada" >}}

The town of Ponta Delgada, the main commercial center and port, is quite charming, featuring sidewalks paved with black and white volcanic stones in distinctive patterns. Many buildings have whitewashed facades with brown-black window trim and reddish pipe roofs. From the cruise terminal, walk ahead to Avenida do Infante Dom Henrique and then left along the harborfront, lined with stores, cafes and restaurants. At the Praca GV Cabal, the square is enclosed on three sides and dominated by whitewashed, triple-arched town gates, trimmed in black volcanic stone, and a handsome, tall, square clock tower. The tourist office is located there.

{{< h2  text="More" >}}

[Tom's guide](https://www.tomsportguides.com/uploads/5/8/5/4/58547429/azores_ponta_delgada_-07-28-2015.pdf)
