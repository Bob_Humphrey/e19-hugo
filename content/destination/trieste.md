---
title: "Trieste"
date: 2018-09-06
visitdate: 2019-05-30
day: "Thu"
draft: false
status: "done"
---

{{< h1  text="Trieste" >}}

{{< h2  text="Apartment" >}}

Via Petronio, 1, Trieste, Friuli-Venezia Giulia 34138, Italy

Checkin: 4 pm

Checkout: 10 am

[reservation](https://www.airbnb.com/trips/v1/f4acc919-507f-4f9e-86ef-ecf0735369fd/ro/RESERVATION2_CHECKIN/HMFBQCHCMB)

The apartment is a 25 minute walk from Trieste Centrale train station.

Eurospar supermarket is a 6 minute walk with the highest and most ratings.

Despar supermarket is a 4 minute walk and has good ratings.

Though parts can be hilly, the city is extremely walkable – you can see most of it on foot. To get to the Risiera, which is a bit outside of the city center, consider a taxi or take advantage of Trieste’s excellent bus service for just a bit over €1.00 per ticket.

{{< h2  text="Bus Station" >}}

Trieste bus station is located adjacent to the train station.

[Website](https://www.autostazionetrieste.it/en-us)

{{< h2  text="Trieste Tourist Office" >}}

Trieste's official tourist information centre is located just off the main square, opposite Harry's Bar and the entrance to the Grand Hotel Duchi d'Aosta. Run by TurismoFVG - the well-organised regional tourist authority for all of Friuli Venezia Giulia - the office has a wealth of information about Trieste and the rest of the region in several languages, including guides organised both by city and theme. Visitors can also purchase FVG cards (good for free or discounted admission to hundreds of sights and venues in across the region), rent audio tours, and get up-to-date info about everything that's going on in town.

{{< h2  text="The Seafront and Piazza Unita d’Italia" >}}

Walk along the water and make your way out onto the stone pier. Molo Audace pier was built from 1743-1751, and named in honor of the first ship of the Italian Navy to arrive in Trieste. The pier is a gathering place for locals and a wonderful spot to watch the sunset!

{{< h2  text="Canal Grande" >}}

This short canal is lined with shops, cafes, and colorful boats. It is a picturesque location.

{{< h2  text="Castle of San Giusto and Trieste Cathedral" >}}

On the hill above the city center is the Castle of San Giusto. The hill top actually has a few things to see, once of which includes great views of the city and a lovely park. There are Roman ruins with a few rows of columns and walls. The Castle has a small museum which is a separate fee than the castle. Although seeing the castle means you can climb on the walls, see the views, and wander through the main courtyard. Next to the castle is the Trieste Cathedral which is an interesting as you can see the span of different centuries outside and within. Note the old stones on the façade, perhaps dating back to the 6th century. Mosaic floors weave into newer floors inside and Gothic rose window decorates the façade. And my favorite, are the 12th-13th century mosaics in the apse.

The Bell Tower

You can also climb the bell tower for more wonderful views of the city and the sea. I highly recommend climbing the bell tower. As you climb, you’ll see be climbing steps that are on the outside of an older tower and you’ll see that the stones that are hundreds of years old. It is quite a unique bell tower climb!

{{< h2  text="Wander through the Old Town" >}}

Meander through the old town, admire the architecture and see the neighborhoods that locals live in. Duck into a few churches which always tell so much about this history of a place through the architecture and art. The Serbian Orthodox Saint Spryridon Church built in the 19th century and a block away from the Canal Grande, is spectacular inside! And while in Trieste make sure to sample the coffee as it is said that it is the “coffee capital of Italy”!

{{< h2  text="Miramare Castle" >}}

PARK IS FREE

The Miramare Castle overlooks the Mediterranean from a large white rocky promontory, so you can’t miss it. The 54 acres of the park envelop the residence of Emperor Maximilian I and Empress Carlota of Mexico.

45 minutes by bus

Take bus number 36 (buy tickets from a tabacchi) to get to Miramare and get off at the last stop. Miramare Castle and gardens are a 10 minute walk from the bus stop.

Miramare Castle is located in Grignano which is a short drive from Trieste.  It’s easy to take the local bus from Trieste to Grignano.  Bus number 6 regularly passes through Trieste centro on the way to Grignano.  I got on the number 6 at this location but check the bus stops closer to your location to see if the number 6 passes.  You must buy the bus ticket ticket from a local new agent before getting on the bus.  I recommend buying 2 tickets, the additional ticket for your return journey.  When you enter the bus you must validate your ticket in the machine.  If you sit on the left hand side, you’ll have a sea view as you travel to the castle.  I also recommend you get off a little before the castle as it’s a beautiful promenade walk.  In addition you can take Instagrammable photos from the promenade (above).  The bus stop closest to Miramare castle is located here.

{{< h2  text="La Risiera di San Sabba" >}}

You shouldn’t visit Trieste without taking a moment to learn about one of the darkest chapters in its history. During WWII the Nazis transformed the old Risiera, or rice mill, into a prison and concentration camp. It was the only Nazi concentration camp with a crematorium in all of Italy. It was used to detain, transport, and kill political prisoners, an estimated 3,000 of whom died here. Though the citizens of Trieste knew that the Nazis had occupied the building and were using it as a prison, the SS officers used loudspeakers and bonfires to block the noise of killing and smell of corpses, purposefully keeping the cremations secret as the war was ending. This museum is a sad stop, but also an important testament to Trieste’s suffering during the war – definitely one of the more profound things to do in Trieste.

More information

The museum is open every day from 9:00am to 7:00pm. Entrance is free. Audioguides cost €2.00 but are highly recommended to understand the more or less empty spaces.

45 minute walk from our apartment. Or take bus n. 8 or 10 to arrive at via Giovanni Palatucci, 5

{{< h2  text="Trieste Cathedral" >}}

The St. Justus Cathedral is the main church in Trieste and an enduring symbol of the city. It was built atop two previous churches in 1300. Outside you’ll find a large Roman rose window and a statue of St. Justus from 1337. Inside are magnificent Byzantine mosaics reminiscent of those in Ravenna. The most impressive is an image of Christ against a gold background dating to the 1200s which you’ll find in the Chapel of San Giusto.

{{< h2  text="Trieste Roman Theater" >}}

Amongst all the modern buildings of Trieste is the Trieste Roman Theater.  The theater dates back to the 1st century and had a capacity of around 6000.  As with many Roman theaters around the world such as nearby Pula Arena, it’s built on a slope.  Something else this theater has in common with Pula is the fact it had a sea view.  Although in the case of Trieste, the sea has receded since the first century and several tall buildings are now blocking the view.  The Trieste Roman Theater is not as impressive as the Colosseum in Rome or Verona Arena but it’s definitely worth viewing.  Viewing the Roman Theater is free and you can view it 24 hours a day.

{{< h2  text="Muggia" >}}

Bus line #20 or take the ferry.

[Ferry info](https://www.triestetrasporti.it/en/time-schedule-and-routes/sea-lines/trieste-muggia/)

Ferry tickets are 4,25 e one way and 8 e round trip.

TripAdvisor review:

Muggia is a lovely little town where to spend a couple of hours walking around the main square and the little harbour. It is full of cafes where to stop and have a drink, and most of the part near the harbour is pedestrian only, making it very enjoyable to walk into.

It is only a short boat ride away from Trieste, if you really want to play the tourist, and it's got plenty of little shops with interesting things for sale.

However, Muggia is not a touristic town in any possible way. The tourist info point is a good 20mins walk away from where the boat leaves you, or even from the bus station. The shops are mostly closed for lunch between 12 and 4pm, and will close around 7 in the evening, this is if you are not there on a Monday or Wednesday morning, when they are just shut. Most shop assistant have rude mannerism and it seems like they are doing you a favour in selling you something.

Despite this, I would recommend a visit if passing through or having a holiday in Trieste.


{{< h2  text="Other" >}}

Piazza della Borsa – You can’t miss the enormous columns of the old Stock Exchange.

Arco di Riccardo – An ancient archway built into the Roman walls.

Victory Lighthouse – Both a lighthouse and monument to the victims of WWI.

Sunsets - With Trieste’s west facing position, it’s worth sticking around for the sunset which can be anything from pale yellow to bright red. The best sunset watching spots are near the waterfront statues in front of Piazza dell’Unita d’Italia or from the Canal Grande.

{{< h2  text="Walks" >}}

{{< h3  text="Walk from Opicina to Miramare Castle" >}}

3 hours, mostly downhill

Take bus #2 from Via Galatti to Opicina. (The historic tram is out of service.)

Opicina – Monte Grisa – Prosecco – Castello di Miramare – Grignano.

The hike starts at the Obelisk of Opicina, reachable from Trieste by bus or by the historic railway cable car. From the lookout point at the Obelisk you have a great view of Trieste and the port facilities. Then it goes high above the Gulf of Trieste, through forest and sometimes through open terrain, to the Monte Grisa with its monumental pilgrimage church. With beautiful views of the coast and the village of Contovello you can reach the karst village Prosecco. From here you descend directly, pass the Miramare train station and reach the parks and Miramare Castle.

{{< h3  text="Passeggiata Rilke" >}}

Take bus #44 from Piazza Oberdan to Sistiana (one hour - 70 stops).

[Timetable](https://www.triestetrasporti.it/en/time-schedule-and-routes/lines-and-timetables/line-44)

The Passeggiata Rilke is arguably one of Italy’s prettiest walks, a cliffside path with incredible views over the Sistiana bay. At just over a mile (or 1.7 km) long, the path starts in front of the tourist office in the town of Sistiana (just north of Trieste) and ends at the Duino castle, famous for its legend about an evil husband who threw his wife over the cliff. Before she hit the water, the gods turned her to stone, reminicent of a veiled lady who can be seen today.

{{< h3  text="Walk from Opicina to Duino" >}}

This is a 16 mile all day hike taken from a tour company brochure. May be able to get
more information from the tourist office.  Lots of villages for rest stops.

Take bus #2 from Via Galatti to Opicina. (The historic tram is out of service.)

You will be picked up from your accommodation in Trieste at 8.30 a.m. to start an active day out exploring the lovely natural environment of the Carso plateau. We will walk from Duino to Opicina along the old roads and trails that used to link the ancient Carso villages of Ceroglie, Malchina, Prepotto, San Pelagio, Ternova, Samatorza, Sales, Sgonico, Rupingrande and finally Opicina. We'll learn why the Carso (Karst), with its surface and underground phenomena, its extraordinary biodiversity and its unique landscape became the scientific term for similar land formations around the world. It's an easy-doing itinerary where you don't need trekking clothes as it's a long walk through woods, meadows dotted with red sumac, and enchanting karstic pathways with panoramic views over the city, the seafront of Istria and the lagoon of Grado. Frequent stops along the way to visit the enchanting little villages and sample local products are also included in this wonderful tour that fills the eyes and warms the heart!

{{< h2  text="Day Trips" >}}

{{< h3  text="Portoroz" >}}

The beautiful promenade walk from Portoroz to Piran takes around 40 minutes.  Once you arrive in Medieval Piran, you’ll appreciate the colorful houses and views of the Gulf of Piran.  The famous postcard photo of Piran is an aerial photo of the buildings surrounded by the sea.  This Instagrammable shot is taken from the Town Walls and a visit to this viewpoint is a must.

90 minutes 7 e one way

[Website](https://www.autostazionetrieste.it/en-us)

It may be easier and cheaper to take a boat to Piran.  Check out Trieste Lines,
Nuova Stazione Marittima – Molo IV - Trieste.  30% discount over 60.  Website
is a mess, but I believe round trip tickets are under 10 e.

{{< h2  text="Articles" >}}

(https://www.nytimes.com/2011/05/01/travel/01trieste-italy.html)

(https://www.wsj.com/articles/a-trip-to-trieste-italys-most-beautifully-haunting-city-1449167192
)

(http://blog.la76.com/2014/01/saturdays-trieste-italy/)

(https://www.rearviewmirror.tv/trieste-italy/)

(https://www.instagram.com/ildrastico/?utm_source=ig_embed)


(https://www.walksofitaly.com/blog/travel-tips/things-to-do-in-trieste)

(http://www.leeabbamonte.com/europe/western_europe/5-awesome-things-to-do-in-trieste-italy.html)

(https://www.thecrazytourist.com/15-best-things-trieste-italy/)
